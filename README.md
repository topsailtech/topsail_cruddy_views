# TopsailCruddyViews
This gem provides default CRUD admin views for ActiveRecord models.
It is meant to work with topsail_cruddy_actions (but could work with other CRUD controllers).

## Installation

add to your *Gemfile*:
```ruby
git_source(:topsail_gem){ |repo_name| "https://bitbucket.org/topsailtech/#{repo_name}.git" }
...
gem 'topsail_cruddy_views', topsail_gem: 'topsail_cruddy_views'

gem 'topsail_common', topsail_gem: 'topsail_common' # for DOM effect javascript modules

gem 'topsail_cruddy_actions', topsail_gem: 'topsail_cruddy_actions' # for boilder plate CRUD actions
```

## Usage
* [Cruddy Views - Single Page](#Cruddy-Views-Single-Page)

### Cruddy Views - Single Page

* In your controller, or a superclass, make sure Current.user is set (e.g. via code below)
```ruby
	    before_action :authenticate_user_profile!
```
* In your controller, or a superclass, call
```ruby
        include CruddyActions
        include CruddyViewsSinglePage

        layout ->{ request.xhr? ? false : nil }
```

* Pin @rails/ujs in your importmap
```sh
        ./bin/importmap pin @rails/ujs
```
* Add to your `app/javascript/application.js`:
```javascript
        import "topsail_cruddy_views/application"
        import Rails from '@rails/ujs';

        Rails.start();
```

* Add to your `app/assets/stylesheets/application.scss`:
```scss
        @use "topsail_cruddy_views";
```

In your view folder you can overwrite any partial for which a default is provided in ```app/views/single_page/application/```
