# Description:
#     Creates a model and associated controllers, views and policies according to topsail_starter resources standards.

#     Namespaces _should_ work as well.


# Example:
#     rails generate ts_resource Thing --parent_controller DictionariesController

#     This will create:
#         app/model/thing.rb
#         app/controllers/things_controller.rb
#         app/policies/thing_policy.rb
#         app/view_components/thing_index_table.rb
#         app/views/things/_index_filter.html.erb
#         app/views/things/_form.html.erb

#         and a route (NOTE: you might have to adjust the namespace here after the generator runs!)

class TsResourceGenerator < Rails::Generators::NamedBase

  class_option :attributes, type: :array, default: [], banner: "field[:type] field[:type]"

  class_option :parent_class,      type: :string, default: "ApplicationRecord",  desc: "The parent class name for the resource"
  class_option :parent_controller, type: :string, default: "ResourcesController", desc: "The parent class name for the controller"
  class_option :parent_policy,     type: :string, default: "ApplicationPolicy",   description: "The parent class name for the policy"

  source_root File.expand_path('../templates', __FILE__)

  def create_ts_resource_files
    template 'model.rb', File.join('app/models', class_path, "#{file_name}.rb")
    template 'controller.rb', File.join('app/controllers', class_path, "#{file_name.pluralize}_controller.rb")
    template 'policy.rb', File.join('app/policies', class_path, "#{file_name}_policy.rb")
    copy_file '_index_filter.html.erb', File.join('app/views', class_path, "#{file_name.pluralize}/_index_filter.html.erb")
    template '_form.html.erb', File.join('app/views', class_path, "#{file_name.pluralize}/_form.html.erb")
    template '_index_cfg.html.rb', File.join('app/views', class_path, "#{file_name.pluralize}/_index_cfg.html.rb")

    route "resources :#{file_name.pluralize} # adjust the module if this resource is in a module!"
  end

  protected
    def parent_class_name
      options[:parent_class]
    end

    def parent_controller_name
      options[:parent_controller]
    end

    def parent_policy_name
      options[:parent_policy]
    end

    def attributes
      # if File.join('app/models', class_path, "#{file_name}.rb").present? # verify model has already been created
        if options[:attributes].present? # take command line arguments, if present
          options[:attributes].map do |a|
            column_name, data_type = a.split(":")
            data_type ||= "string"
            [column_name, data_type, (data_type == "references" ? column_name + "_id" : column_name)]
          end
        else
          audit_cols = ["created_by_id", "updated_by_id", "created_at", "updated_at"]
          # skip audit_cols when pulling attributes directly from the model
          # require_dependency("app/models/" + file_name + ".rb")
          connection = ActiveRecord::Base.connection
          sql = "SELECT column_name, data_type
                 FROM   information_schema.columns
                 WHERE  table_name = '#{file_name}'
                 ORDER  BY ordinal_position;"
          result = connection.execute(sql)
          result = result == 0 ? result : result.to_a

          if result.is_a?(Array)
            attributes_array = result.map{|hash| [ hash["column_name"], hash["data_type"] ] unless audit_cols.include?(hash["column_name"]) }.compact.uniq

            # convert to railsy data_types
            attributes_array.map{|field_name, data_type| [field_name, convert_psql_data_type(data_type), (data_type == "references" ? field_name + "_id" : field_name)] }
          else
            []
          end
        end
      # end
    end

    def column_names
      attributes.map(&:last).sort
    end

    def convert_psql_data_type(data_type)
      if data_type == "character varying"
        "string"
      elsif data_type == "numeric"
        "decimal"
      elsif data_type == "bigint"
        "integer"
      elsif data_type.include?("timestamp")
        "datetime"
      else
        data_type
      end
    end


    def form_field_tpl_els
      attributes.map do |attribute_name, data_type|
        data_type ||= "string"
        field_name = data_type == "references" ? attribute_name + "_id" : attribute_name
        field_name_stem = attribute_name.gsub("_id", "")

        if key?(field_name)
          "<%= f.top_label(:#{field_name}, style: 'width: 15em;'){
            f.select(:#{field_name},
              [],
              { include_blank: true },
              is: 'ts-select2',
              'data-ajax--url' => #{field_name_stem.pluralize}_path(format: 'json', variant: 'select2', sort: :name))} %>"
        else
          case data_type
          when "string"
            "<%= f.top_label(:#{field_name}, style: 'width: 20em;'){ f.text_field(:#{field_name}) } %>"
          when "date"
            "<%= f.top_label(:#{field_name}){ f.date_field(:#{field_name}) } %>"
          when "datetime"
            "<%= f.top_label(:#{field_name}){ f.datetime_field(:#{field_name}) } %>"
          when "decimal"
            "<%= f.top_label(:#{field_name}){ f.number_field(:#{field_name}, step: :any) } %>"
          when "money"
            "<%= f.top_label(:#{field_name}){ f.number_field(:#{field_name}, step: :any) } %>"
          when "integer"
            "<%= f.top_label(:#{field_name}){ f.number_field(:#{field_name}) } %>"
          when "boolean"
            "<%= f.top_label(:#{field_name}){ f.check_box(:#{field_name}) } %>"
          when "text"
            "<%= f.top_label(:#{field_name}, style: 'font-size: 1em; width: 30em; height: 8em;'){ f.text_field(:#{field_name}) } %>"
          else
            "<%= { f.text_field(:#{field_name}) } %>"
          end
        end
      end
    end

    def controller_index_table_column_tpl_els
      if attributes.empty?
        ["id: ->(row){ row.id },"]
      else
        column_names.without(:id).map do |column_name|
          "#{column_name}: ->(row){ row.#{column_name} },"
        end
      end
    end

    def model_will_filter_tpl_els
      key_columns.map do |column_name|
        "#{column_name}: ->(i){ where #{column_name}: i }"
      end
    end

    def key_columns
      if attributes.empty?
        ["id"]
      else
        ["id"] + column_names.select(&method(:key?))
      end
    end

    def key?(column_name)
      column_name.end_with?("_id")
    end
end
