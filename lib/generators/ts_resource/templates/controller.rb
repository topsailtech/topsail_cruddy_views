<% module_namespacing do -%>
# frozen_string_literal: true

  class <%= class_name.pluralize %>Controller < <%= parent_controller_name.classify %>
  end
<% end -%>
