<% module_namespacing do -%>
# frozen_string_literal: true

class <%= class_name %>Policy < <%= parent_policy_name.classify %>
  # def manage?
  #   true
  # end

  def permitted_attributes
    <%= column_names.map(&:to_sym).without(:id) %>
  end

  # class Scope < Scope
  #   def resolve
  #     scope
  #   end
  # end
end
<% end -%>
