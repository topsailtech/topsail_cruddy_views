<% module_namespacing do -%>
# frozen_string_literal: true

class <%= class_name %> < <%= parent_class_name.classify %>
  will_sort default: :id,
            id:      ->(){ order(:id) }


  will_filter <%= model_will_filter_tpl_els.join(",\n              ") %>


end
<% end -%>
