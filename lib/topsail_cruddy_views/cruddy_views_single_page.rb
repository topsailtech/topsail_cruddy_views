module CruddyViewsSinglePage
  extend ActiveSupport::Concern

  included do
    before_action :add_view_paths

    private

    def add_view_paths
      prepend_view_path File.dirname(__dir__) + '../../app/views/single_page'
    end
  end
end
