module TopsailCruddyViews
  # engine mounting behavior
  class Engine < ::Rails::Engine
    config.app_generators.ts_resource :ts_resource

    initializer 'topsail-cruddy-views.importmap', before: 'importmap' do |app|
      app.config.importmap.paths << Engine.root.join('config/importmap.rb')
    end
  end
end
