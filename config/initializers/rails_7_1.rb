gt_rails70 = Rails::VERSION::MAJOR > 7 || (Rails::VERSION::MAJOR == 7 && Rails::VERSION::MINOR.positive?)
if gt_rails70
  # Fix .ruby template handler to evaluate entire file content.
  # Somehow Rails changed the handler between Rails 7.0 and 7.1
  ActionView::Template.register_template_handler(:ruby, ->(_, source) { source.inspect })
end
