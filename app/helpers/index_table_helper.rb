module IndexTableHelper
  # @param formats [Array | nil] allows to fetch definitions for request formats other than the current request's format
  def index_column_definitions(formats: nil)
    evaluate_plain_ruby_partial 'index_cfg', formats: formats # .ruby handler to return plain ruby
  end

  def index_total_definitions
    evaluate_plain_ruby_partial 'index_total_cfg' # .ruby handler to return plain ruby
  end

  # helper if you need to render a one-off index row
  def index_cell_procs(index_cfg_file = 'index_cfg')
    evaluate_plain_ruby_partial(index_cfg_file).values
  end

  def sort_header(records, attribute)
    link_inner = records.nil? || attribute.blank? ? attribute : records.model.human_attribute_name(attribute)
    link_inner = link_inner.html_safe if attribute&.ends_with?('_html')

    content_tag :th do
      if records.sortable_by?(attribute)
        index_list_sort_link(records, attribute, title: link_inner)
      else
        link_inner
      end
    end
  end

  private

  def evaluate_plain_ruby_partial(partial_template, formats: nil)
    result = render(partial: partial_template, formats: formats)
    if Rails::VERSION::MAJOR > 7 || (Rails::VERSION::MAJOR == 7 && Rails::VERSION::MINOR.positive?)
      # starting Rails 7.1 the ruby tamplate handler doesn't run the ruby code anymore
      eval(result)
    else
      result
    end
  end

  def index_list_sort_link(records, attribute, title:)
    current_multi_sort_index = records.sort_index(attribute) || -1

    atts = {
      href: '#',
      data: {
        sort_by: attribute,
        action:  'click->cruddy-view#primarySort contextmenu->cruddy-view#subsequentSort'
      }
    }
    if records.sort_dir(attribute)
      atts[:data][:sort_dir] = records.sort_dir(attribute) # important to JS controller
      if current_multi_sort_index.positive? # subsequentSort
        atts[:'sortpos'] = current_multi_sort_index + 1 # only important for CSS
      end
    end
    tag.a title, **atts
  end
end
