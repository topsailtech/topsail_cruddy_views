import { Controller } from "@hotwired/stimulus"
import { backgroundFlash as highlight } from "topsail_common/libs/effect"
import { info as growlInfo } from "topsail_common/libs/growl"
import { serializeElement as serializeElement } from "topsail_common/libs/serialize"

export default class extends Controller {
  static targets = [
    "indexFilter",
    "indexList",
    "newRecords", // the DOM where newly created records will be appended
    "indexTotals" // the "row" holding index totals
  ]

  static values = {
    prependsNewRecords: Boolean,  // should newly created records prepended to newRecords target insetad of appended; default FALSE
    indexTotalsPath:    String,   // (optional) url for generating the index totals "row"; we'll send the current filter to it to fetch new totals
    filterMatchPath:    String,   // (optional) url for testing if a record matches the current filter; must return true or false as JSON
    initiallySubmitIndexFilter: { type: Boolean, default: true } // on page load, should we go and send and ajaxy fetch for the rows
  }

  connect(){
    if (this.initiallySubmitIndexFilterValue){
      this.submitIndexFilter()
    }
  }

  indexListFetching(evt){
    this.indexListTarget.style.opacity = 0.3
  }

  indexListFetched(evt){
    const [_data, _status, xhr] = evt.detail

    this.indexListTarget.style.opacity = 1
    this.indexListTarget.innerHTML = xhr.responseText
    this.indexFilterTarget.querySelectorAll('input[name="paginate[page]"]').
      forEach(i => i.remove())
  }

  new(evt){
    openModal(this.element, evt.detail[2].responseText)
  }

  show(evt){
    openModal(this.element, evt.detail[2].responseText)
  }

  edit(evt){
    markDataRowForEdit(evt.target, true)
    openModal(evt.target.parentElement, evt.detail[2].responseText)
  }

  cancel(evt){
    evt.preventDefault()
    markDataRowForEdit(evt.target, false)
    evt.target.closest("dialog")?.remove()
  }

  // The resource form has been submitted, and we got an answer. Close the dialog and update the view.
  // This is also being called on
  // - updates
  // - create (closes DIALOG only if _inserting_ new DOM fragment; you can build wizard by just updating the DIALOG)
  // - destroy
  // - failures (for any of the actions above)
  update(ajax_complete_evt){
    const [xhr, _status] = ajax_complete_evt.detail,
          success = (xhr.status >= 200 && xhr.status < 300)
    if (success){
      markDataRowForEdit(ajax_complete_evt.target, false)
      document.getElementById(xhr.getResponseHeader("ts-deleted"))?.remove() // remove record, incl. DIALOG inside
    }

    const placements = replaceDom(xhr.responseText),
          unplaced = placements.unplaced
    if (unplaced.length > 0) {
      if (this.hasNewRecordsTarget && xhr.status == 201){ // CREATE
        this.newRecordsTarget[this.prependsNewRecordsValue ? 'prepend' : 'append'](...unplaced)
        ajax_complete_evt.target.closest("dialog")?.remove()
      } else {
        console.error("don't know where to place fetched DOM elements", unplaced)
      }
    }

    if (success){
      growl(xhr)
      this.reloadTotals()
      this.checkFilterMatch(placements.placed[0] || unplaced[0]) // guessing it's the first updated or created element
    }
  }
  // just helpers if you feel uneasy to use :update directly
  create(ajax_complete_evt){ this.update(ajax_complete_evt) }
  destroy(ajax_complete_evt){ this.update(ajax_complete_evt) }

  // just replace the matching DOM elements with what we got back.
  // This is e.g. useful when building a multi-step wizard, and replacing content within the modal dialog.
  // Also useful for inline actions.
  replaceDom(ajax_complete_evt){
    const [xhr, _status] = ajax_complete_evt.detail
    const unplaced = replaceDom(xhr.responseText).unplaced
    if (unplaced.length > 0) {
      console.error("don't know where to place fetched DOM elements", unplaced)
    }
  }

  // The evt.target is expected to have a data-sort-by attribute.
  // If currently sorting by that attribute, the evt.target will
  // also have an data-sort-dir attribute ("asc" or "desc")
  primarySort(evt){
    evt.preventDefault()
    this._unsortBy()
    this.indexFilterTarget.append(hiddenInput("sort[]", sortVal(evt.target)))
    this.submitIndexFilter()
  }

  // See expected properties of evt.target in primarySort()
  subsequentSort(evt){
    evt.preventDefault()
    this._unsortBy(evt.target.dataset.sortBy)
    this.indexFilterTarget.append(hiddenInput("sort[]", sortVal(evt.target)))
    this.submitIndexFilter()
  }

  flipPage(evt){
    evt.preventDefault()
    const page = evt.target.tagName == "A" && evt.target.dataset?.page

    if (page){
      this.indexFilterTarget.append(hiddenInput("paginate[page]", page))
      this.submitIndexFilter()
    }
  }

  pushState(evtOnForm){
    const url = evtOnForm.target.action.split('?')[0],
          query = serializeElement(evtOnForm.target)
    history.replaceState({}, "", `${url}?${query}`)
  }

  _unsortBy(sortAttribute = null){
    if (sortAttribute){
      this.indexFilterTarget.
        querySelectorAll(`input[name="sort[]"][value^="${sortAttribute} "] ~ input[name="sort[]"]`).
        forEach(i => i.remove())
      this.indexFilterTarget.
        querySelector(`input[name="sort[]"][value^="${sortAttribute} "]`)?.
        remove()
    } else {
      this.indexFilterTarget.querySelectorAll('input[name="sort[]"]').
        forEach(i => i.remove())
    }
  }

  submitIndexFilter(evt){
    if (!this.hasIndexFilterTarget ||
                                          // ignore events from UI helpers
        evt?.target?.closest("div.ts-wrapper") || // tom-select
        evt?.target?.closest("div.ss-main")       // slim-select
       ){ return }

    fire(this.indexFilterTarget, "submit")
  }

  // @return the current filter field values as a string for GET
  _filterValueGetParams(){
    return Array.from(new FormData(this.indexFilterTarget).entries()).
      map( p => `${p[0]}=${p[1]}` ).
      join("&")
  }

  reloadTotals(){
    if (this.indexTotalsPathValue && this.hasIndexTotalsTarget){
      fetch(this.indexTotalsPathValue + "?" + this._filterValueGetParams()).
      then(response => response.text()).
      then(html => this.indexTotalsTarget.outerHTML = html)
    }
  }

  // @param rowDom[HTMLElement] - the "row" to check. It must have an ID that is in Rails dom_id format
  checkFilterMatch(rowDom){
    if (!rowDom?.id || !this.hasFilterMatchPathValue) return

    let recordId = rowDom.id.match(/\d*$/)[0]
    fetch(`${this.filterMatchPathValue}?id=${recordId}&${this._filterValueGetParams()}`).
      then(response => response.json()).
      then(json => rowDom.classList.toggle("disqualified", !json))
  }
}

function openModal(dialogParent, dialogContent){
  const dialog = document.createElement("dialog");
  dialog.classList.add("cruddy_view_edit")
  dialog.addEventListener("cancel", ()=>dialog.remove())
  dialogParent.append(dialog)
  dialog.innerHTML = dialogContent
  dialog.showModal()
}

// @return [HTMLInputElement] - unattached with type=hidden and requested name + value
function hiddenInput(name, value){
  let el = document.createElement("input")
  el.type = "hidden"
  el.name = name
  el.setAttribute("value", value) // must be attribute, so that we can CSS select it
  return el
}

// @param a link or a decendant with "sort-by" and optional "sort-dir" data
// @return [String] - for sortLink, return sort fragment incl. sort direction
function sortVal(sortLink){
  const actualSortLink = sortLink.closest("[data-sort-by]"),
        sortDir = actualSortLink.dataset.sortDir?.toLowerCase() == 'asc' ? 'desc' : 'asc'
  return `${actualSortLink.dataset.sortBy} ${sortDir}`
}

function growl(xhr){
  const text = xhr.getResponseHeader("Content-Description")

  text && growlInfo(text);
}

// need to possibly tell a datarow that we want to forget about hver styles if dialof inside is open
function markDataRowForEdit(elInRow, force){
  elInRow.closest("table.data_table > tbody > tr")?.
    classList.toggle("showing_form", force)
}

// For each root element in htmlFragment, replace existing DOM elements with the same id.
// @return Hash with keys "placed" and "unplaced", values are Array - root elements of htmlFragment that could/ could not be placed
function replaceDom(htmlFragment){
  let placed = new Array(),
      unplaced = new Array()
  for (let newDomEl of elementsFromString(htmlFragment)){
    if (newDomEl.id){
      let existing = document.getElementById(newDomEl.id)
      if (existing){
        existing.parentElement.replaceChild(newDomEl, existing)
        placed.push(newDomEl)
      } else {
        unplaced.push(newDomEl)
      }
    } else {
      console.error("All root elements must have an id attribute!")
      unplaced.push(newDomEl)
    }
    highlight(newDomEl)
  }
  return { unplaced: unplaced, placed: placed }
}

function elementsFromString(htmlFragment){
  let root = document.createElement("template") // using "template" so that TDs etc. will work too
  root.innerHTML = htmlFragment
  return Array.from(root.content.children) // Return Array instead of HTMLCollection to make it not "live"
}

function fire(obj, name, data) {
  const event = new CustomEvent(name, { bubbles: true, cancelable: true, detail: data });
  obj.dispatchEvent(event);
  return !event.defaultPrevented;
}
