import { application } from "controllers/application"

import CruddyViewController from "topsail_cruddy_views/controllers/cruddy_view_controller";
application.register("cruddy-view", CruddyViewController);
