module TopsailCruddyViews
  # This is like an OpenStruct that keeps track of which keys have been accessed.
  # Once a key has been accessed, that key will removed from :unaccessed_keys array.
  # You have to provide a model_class, so that form builders on this ModelFilterAccounted
  # can properly look up I18n keys.
  # If you have nested attributes, you can set alternative model classes for I18n, by setting
  # an entry in i18n_models_for_key. The existence of such entry also eensures such nested attribute.
  class ModelFilterAccounted
    attr_reader :i18n_models_for_key
    attr_writer :to_model_returns_self

    def initialize(filter_hash, model_class:)
      @filter_hash = (filter_hash || {}).with_indifferent_access # stored with String keys
      @accessed_keys = []
      @model_class = model_class
      @to_model_for_i18n = model_class.new
      @i18n_models_for_key = {}.with_indifferent_access
      self.to_model_returns_self = self.class.form_with_sends_to_model?
    end

    def method_missing(method_name, *_args, &_block)
      return if !nested_key?(method_name) && !@filter_hash.key?(method_name)

      @accessed_keys << method_name.to_s
      self[method_name]
    end

    def respond_to_missing?(method_name, include_private = false)
      @filter_hash.keys.include?(method_name.to_s) || nested_key?(method_name) || super
    end

    def to_model
      @to_model_returns_self ? self : @to_model_for_i18n
    end
    delegate :model_name, to: :@to_model_for_i18n

    def unaccessed_keys
      (@filter_hash.keys - @accessed_keys).reject { |key| self[key].nil? }
    end

    def [](key)
      val = @filter_hash[key]
      return val unless nested_key?(key)

      self.class.new(val, model_class: @i18n_models_for_key[key] || @model_class)
    end

    def []=(key, value)
      @filter_hash[key] = value
    end

    # Starting Rails 7.1beta1 the `form_with` helper sends `to_model` to the builder's object.
    # For that reason we have to return `self`. But i18n needs `to_model` to return the
    # underlying model_class instance. So we are switching that result once we are inside the form builder in Rails 7.1+
    def self.form_with_sends_to_model?
      Rails::VERSION::MAJOR > 7 || (Rails::VERSION::MAJOR == 7 && Rails::VERSION::MINOR > 0)
    end

    private

    def nested_key?(key)
      i18n_models_for_key.key?(key) || @filter_hash[key].is_a?(Hash)
    end
  end
end
